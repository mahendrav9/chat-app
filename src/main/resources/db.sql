CREATE TABLE `chat_db`.`chat_history` (
  `user_name` VARCHAR(45) NOT NULL,
  `chat_room` VARCHAR(45) NULL,
  `chat_message` VARCHAR(200) NULL,
  `date_time` DATETIME NULL,
  PRIMARY KEY (`user_name`));
