package com.hcl.ing.testapp.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

public class TestDate {

	private static final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

	public static void main(String[] args) {

		java.util.Date dat = new java.util.Date();
		System.out.println(sdf.format(dat));

		java.util.Calendar cal = java.util.Calendar.getInstance();
		System.out.println(sdf.format(cal.getTime()));

		java.time.LocalDateTime now = java.time.LocalDateTime.now();
		System.out.println(dtf.format(now));

		java.time.LocalDate localDate = java.time.LocalDate.now();
		System.out.println(DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate));

		java.util.Date date = new java.util.Date();
		System.out.println(date);
		System.out.println(new java.sql.Date(date.getTime()));
		System.out.println("sql date ---"+sdf.format(new java.sql.Date(date.getTime())));

		java.util.Date datee = new java.util.Date();
		System.out.println(datee);
		System.out.println("Timestamp ---"+sdf.format(new java.sql.Timestamp(datee.getTime())));
		
		java.util.Date datt = new java.util.Date();
		System.out.println(datt);
		System.out.println("sql Time ---"+sdf.format(new java.sql.Time(datt.getTime())));
		
		
		

	}

}
