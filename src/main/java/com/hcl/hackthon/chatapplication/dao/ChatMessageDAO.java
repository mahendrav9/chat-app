/**
 * 
 */
package com.hcl.hackthon.chatapplication.dao;

import java.util.List;

import com.hcl.hackthon.chatapplication.model.ChatMessageRequest;
import com.hcl.hackthon.chatapplication.model.ChatMessageResponse;

/**
 * @author Hackathon
 *
 */
public interface ChatMessageDAO {
	
	public List<String> getChatRooms();

	public List<ChatMessageResponse> saveChat(ChatMessageRequest chatMessageRequest);

}
