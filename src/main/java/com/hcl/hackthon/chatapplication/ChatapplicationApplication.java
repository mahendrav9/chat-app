package com.hcl.hackthon.chatapplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication 
public class ChatapplicationApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChatapplicationApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(ChatapplicationApplication.class, args);
	}
}
