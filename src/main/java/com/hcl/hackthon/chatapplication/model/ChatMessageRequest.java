package com.hcl.hackthon.chatapplication.model;

/*
 * ChatMessageRequest
 */
public class ChatMessageRequest {

	private String userName;
	private String chatRoom;
	private String chatMessage;

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the chatRoom
	 */
	public String getChatRoom() {
		return chatRoom;
	}

	/**
	 * @param chatRoom
	 *            the chatRoom to set
	 */
	public void setChatRoom(String chatRoom) {
		this.chatRoom = chatRoom;
	}

	/**
	 * @return the chatMessage
	 */
	public String getChatMessage() {
		return chatMessage;
	}

	/**
	 * @param chatMessage
	 *            the chatMessage to set
	 */
	public void setChatMessage(String chatMessage) {
		this.chatMessage = chatMessage;
	}

}
