package com.hcl.hackthon.chatapplication.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.hackthon.chatapplication.model.ChatMessageRequest;
import com.hcl.hackthon.chatapplication.model.ChatMessageResponse;
import com.hcl.hackthon.chatapplication.model.ChatRooms;
import com.hcl.hackthon.chatapplication.service.ChatMessageService;

/*
 * ChatMessageController
 */
@RestController
@CrossOrigin("*")
public class ChatMessageController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChatMessageController.class);

	@Autowired
	ChatMessageService chatMessageService;

	@RequestMapping(value = "/chatroom", method = RequestMethod.GET, produces = "application/json")
	public List<String> getChatRooms() {

		LOGGER.info("..........getChatRooms...........");

		return chatMessageService.getAllChatRooms();

	}

	@RequestMapping(value = "/chatroom/{roomname}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public List<ChatMessageResponse> getMessgae(@RequestBody ChatMessageRequest chatMessageRequest) {

		/*
		 * DateTimeFormatter dtf = DateTimeFormatter.ofPattern(
		 * "yyyy/MM/dd HH:mm:ss"); LocalDateTime now = LocalDateTime.now();
		 * 
		 * List<ChatMessageResponse> chatMessageResponses = new
		 * ArrayList<ChatMessageResponse>(); ChatMessageResponse
		 * chatMessageResponse = new ChatMessageResponse();
		 * chatMessageResponse.setSenderName(chatMessageRequest.getUserName());
		 * chatMessageResponse.setChatRoom(chatMessageRequest.getChatRoom());
		 * chatMessageResponse.setChatMessage(chatMessageRequest.getChatMessage(
		 * )); chatMessageResponse.setDateTime(now.toString());
		 * chatMessageResponses.add(chatMessageResponse);
		 */
		return chatMessageService.saveChat(chatMessageRequest);

	}

	@RequestMapping(value = "/chatroom/{roomName}", method = RequestMethod.GET, produces = "application/json")
	public List<ChatMessageResponse> getChatRoomMessage(@PathParam(value = "roomName") String room) {

		LOGGER.info("..........getChatRooms...........");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();

		List<ChatMessageResponse> chatMessageResponses = new ArrayList<ChatMessageResponse>();
		ChatMessageResponse chatMessageResponse = new ChatMessageResponse();
		chatMessageResponse.setSenderName("user1");
		chatMessageResponse.setChatRoom("web devlopment");
		chatMessageResponse.setChatMessage("hello");
		chatMessageResponse.setDateTime(now.toString());
		chatMessageResponses.add(chatMessageResponse);

		ChatMessageResponse chatMessageResponse1 = new ChatMessageResponse();
		chatMessageResponse1.setSenderName("user2");
		chatMessageResponse1.setChatRoom("web devlopment");
		chatMessageResponse1.setChatMessage("hello");
		chatMessageResponse1.setDateTime(now.toString());
		chatMessageResponses.add(chatMessageResponse1);

		return chatMessageResponses;

	}

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String getHello() {

		return "hello world";

	}

}
