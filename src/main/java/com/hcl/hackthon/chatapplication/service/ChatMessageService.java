/**
 * 
 */
package com.hcl.hackthon.chatapplication.service;

import java.util.List;

import com.hcl.hackthon.chatapplication.model.ChatMessageRequest;
import com.hcl.hackthon.chatapplication.model.ChatMessageResponse;

/**
 * @author Hackathon
 *
 */
public interface ChatMessageService {

	public List<String> getAllChatRooms();

	public List<ChatMessageResponse> saveChat(ChatMessageRequest chatMessageRequest);

}
